# MUSICA :

Une application en architecture microservice pour une bibliotheque de musiques

---

## MUSICA NEST

- La liste des musiques et des albums
- Une base de donné MongoDb

## Test :

- Utilisation d'une base de donné en memoire : <b>MongoseMemory</b>
- Test Unitaire :
  - Ce fait avec Junit , chaque controlleur à un fichier <b>name.controller.spec.ts</b> qui contient les test
- Test d'integration :
  - Ce fait dans le dossier : test

## Deploiement :

- Docker :
  - Dockerfile
- Kubernetes :
  - deploy.yml
  - service.yml

## URI
<table>
  <thead>
        <tr>
            <th>Description</th>
            <th>METHODE</th>
            <th>URI</th>
            <th>BODY</th>
            <th>Response</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Pour creer un album </td>
            <td>POST</td>
            <td>album</td>
            <td>
            {
              "titre":"Liens du 100",
              "dateDeSortie":"2023-01-01",
              "singer":[
                  "SDM"
              ],
              "descritption": "LE DERNIER ALBUM DE SDM"
            }
            </td>
        <tr>
        <tr>
            <td>Pour mettre à jour un album</td>
            <td>PUT</td>
            <td>album/{id}</td>
            <td>
            {
                "titre":"Liens du 100",
                "dateDeSortie":"2023-01-01",
                "artistes":[
                    "SDM"
                ],
                "descritption": "LE DERNIER ALBUM DE SDM"
            }
            </td>
        <tr>
        <tr>
            <td>Pour prendre la liste des albums</td>
            <td>GET</td>
            <td>album</td>
            <td>
            </td>
        <tr>
        <tr>
            <td>Pour un album</td>
            <td>GET</td>
            <td>album/{id}</td>
            <td>
            </td>
        <tr>
        <tr>
            <td>Pour supprimer</td>
            <td>DELETE</td>
            <td>album/{id}</td>
            <td>
            </td>
        <tr>
        <tr>
            <td>Pour creer une music</td>
            <td>POST</td>
            <td>music</td>
            <td>
            {
              "title":"Paris",
              "dateOut":"2018-01-01",
              "singer":[
                  "Nsika"
              ],
            "albumId":"f3a33874-b47e-436a-8b6b-49d66ab8c381"
          }
            </td>
        <tr>
        <tr>
            <td>Pour mettre à jour un music</td>
            <td>PUT</td>
            <td>music/{id}</td>
            <td>
            {
              "title":"Paris",
              "dateOut":"2018-01-01",
              "singer":[
                  "Niska"
              ],
    "albumId":"f3a33874-b47e-436a-8b6b-49d66ab8c381"}
            </td>
        <tr>
        <tr>
            <td>Pour prendre la liste des albums</td>
            <td>DELETE</td>
            <td>music</td>
            <td>
            </td>
        <tr>
        <tr>
            <td>Pour un album</td>
            <td>DELETE</td>
            <td>music/{id}</td>
            <td>
            </td>
        <tr>
    </tbody>
</table>
