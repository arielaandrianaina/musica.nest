/* eslint-disable prettier/prettier */

import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AlbumController } from '../../Controller/album/album.controller';
import { AlbumRepository } from '../../Repositories/album.repository';
import { Album, AlbumSchema } from '../../Schemas/album.schema';
import { AlbumService } from '../../Services/album/album.service';
import { MusicService } from 'src/Services/music.service';
import { MusicRepository } from 'src/Repositories/music.repository';
import { MusicModule } from '../music.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Album.name, schema: AlbumSchema }]),
    MusicModule,
  ],
  providers: [AlbumService, AlbumRepository],
  controllers: [AlbumController],
})
export class AlbumModule {}
