/* eslint-disable prettier/prettier */
import { IsEmpty, IsNotEmpty, IsString, MaxLength } from 'class-validator';
import { MusicDto } from '../music.dto';
export class AlbumDto {
  @IsString()
  @MaxLength(200)
  @IsNotEmpty()
  titre: string;

  @IsNotEmpty()
  dateDeSortie: Date;

  @IsNotEmpty()
  artistes: string[];

  @IsString()
  @MaxLength(200)
  @IsNotEmpty()
  description: string;

  @IsEmpty()
  musics: MusicDto[];
  @IsEmpty()
  musics_str: string[];
}
