/* eslint-disable prettier/prettier */
import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  Param,
  Post,
  Put,
  UploadedFiles,
  UseInterceptors,
} from '@nestjs/common';
import { AlbumDto } from '../../DTO/album/album.dto';
import { AlbumService } from '../../Services/album/album.service';
import { Album } from '../../Schemas/album.schema';
import { FileFieldsInterceptor } from '@nestjs/platform-express';

@Controller('album')
export class AlbumController {
  constructor(private readonly albumService: AlbumService) {}

  @Post()
  @HttpCode(201)
  @UseInterceptors(
    FileFieldsInterceptor([{ name: 'cover', maxCount: 1 }, { name: 'musics' }]),
  )
  async createAlbum(
    @UploadedFiles()
    files: { cover: Express.Multer.File[]; musics: Express.Multer.File[] },
    @Body() createAlbumDto: AlbumDto,
  ): Promise<Album> {
    return this.albumService.createAlbum(
      createAlbumDto,
      files.cover[0],
      files.musics,
    );
  }

  @Put(':id')
  @HttpCode(204)
  @UseInterceptors(FileFieldsInterceptor([{ name: 'cover', maxCount: 1 }]))
  async updateAlbum(
    @Param('id') id: string,
    @Body() updateAlbumDto: AlbumDto,
    @UploadedFiles()
    files: { cover: Express.Multer.File[] | null },
  ): Promise<void> {
    this.albumService.updateAlbum(
      id,
      updateAlbumDto,
      files.cover == null ? null : files.cover[0],
    );
    return;
  }

  @Get()
  @HttpCode(200)
  async getAllalbum(): Promise<Album[]> {
    return this.albumService.getAllAlbums();
  }

  @Get(':id')
  @HttpCode(200)
  async getAlbum(@Param('id') id: string): Promise<Album> {
    console.log(id);
    return this.albumService.getAlbum(id);
  }

  @Delete(':id')
  @HttpCode(200)
  async deleteAlbum(@Param('id') id: string): Promise<Album> {
    return this.albumService.deleteAlbum(id);
  }
}
