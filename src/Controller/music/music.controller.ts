/* eslint-disable prettier/prettier */
import {
  Body,
  Controller,
  Get,
  Delete,
  HttpCode,
  Param,
  Post,
  Put,
  UploadedFiles,
  UseInterceptors,
} from '@nestjs/common';
import { MusicDto } from '../../DTO/music.dto';
import { MusicService } from '../../Services/music.service';
import { Music } from '../../Schemas/music.schema';
import { FileFieldsInterceptor } from '@nestjs/platform-express';

@Controller('music')
export class MusicController {
  constructor(private readonly musicService: MusicService) {}
  @Get()
  @HttpCode(200)
  async getMusics(): Promise<Music[]> {
    return this.musicService.getMusics();
  }
  @Get('album/:albumId')
  @HttpCode(200)
  async getMusicsByAlbumId(
    @Param('albumId') albumId: string,
  ): Promise<Music[]> {
    return this.musicService.getMusicsByAlbumId(albumId);
  }

  @Get(':id')
  @HttpCode(200)
  async getMusic(@Param('id') id: string): Promise<Music> {
    return this.musicService.getMusicById(id);
  }

  @Post()
  @HttpCode(201)
  @UseInterceptors(
    FileFieldsInterceptor([{ name: 'cover', maxCount: 1 }, { name: 'musics' }]),
  )
  async createMusic(
    @UploadedFiles()
    files: { cover: Express.Multer.File[]; musics: Express.Multer.File[] },
    @Body() body: MusicDto,
  ): Promise<Music> {
    return this.musicService.createMusic(body, files.cover[0], files.musics[0]);
  }

  @Put(':id')
  @HttpCode(200)
  @UseInterceptors(
    FileFieldsInterceptor([{ name: 'cover', maxCount: 1 }, { name: 'musics' }]),
  )
  async updateMusic(
    @Param('id') id: string,
    @Body() body: MusicDto,
    @UploadedFiles()
    files: {
      cover: Express.Multer.File[] | null;
      musics: Express.Multer.File[] | null;
    },
  ): Promise<void> {
    this.musicService.updateMusic(
      id,
      body,
      files.cover == null ? null : files.cover[0],
      files.musics == null ? null : files.musics[0],
    );
    return;
  }

  @Delete(':id')
  @HttpCode(200)
  async deleteMusic(@Param('id') id: string): Promise<Music> {
    return this.musicService.deleteMusic(id);
  }
}
