/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './Controller/app.controller';
import { AppService } from './app.service';
import { MusicModule } from './Modules/music.module';
import { AlbumModule } from './Modules/album/album.module';
import { MulterModule } from '@nestjs/platform-express';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';

let stringConnection = 'mongodb://mahatoky:lsatvph@localhost:27017';
let databaseName = 'Musica';
if ('MONGODB_CONNECTION' in process.env)
  stringConnection = process.env['MONGODB_CONNECTION'];
if ('DATABASE_NAME' in process.env) databaseName = process.env['DATABASE_NAME'];
let pathUPLOAD = join(__dirname, '..', 'files');
if ('FILES_PVC' in process.env) pathUPLOAD = process.env['FILES_PVC'];
@Module({
  imports: [
    MongooseModule.forRoot(stringConnection, { dbName: databaseName }),
    MusicModule,
    AlbumModule,
    MulterModule.register({
      dest: './files',
    }),
    ServeStaticModule.forRoot({
      rootPath: pathUPLOAD,
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
