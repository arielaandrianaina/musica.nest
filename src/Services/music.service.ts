/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { MusicDto } from '../DTO/music.dto';
import { v4 as uuidv4 } from 'uuid';
import { MusicRepository } from '../Repositories/music.repository';
import {
  plainToClass,
  plainToClassFromExist,
  plainToInstance,
} from 'class-transformer';
import * as fs from 'fs';
import { Music } from '../Schemas/music.schema';
import { join } from 'path';

let pathUPLOAD = 'D:\\Musica.P2\\Musica.Nest\\files\\';
if ('FILES_PVC' in process.env) pathUPLOAD = process.env['FILES_PVC'];
@Injectable()
export class MusicService {
  constructor(private readonly musicRepository: MusicRepository) {}
  async getMusicById(musicId: string): Promise<Music> {
    const music = await this.musicRepository.findOne({ musicId });
    // const dto = plainToClass(MusicDto, music);
    return music;
  }

  async getMusics(): Promise<Music[]> {
    const music = await this.musicRepository.find({});
    // const dto = plainToClass(MusicDto, music);
    return music;
  }
  async getMusicsByAlbumId(albumId: string): Promise<Music[]> {
    const musics = await this.musicRepository.find({ albumId });
    return musics;
  }

  async createMusic(
    musicDto: MusicDto,
    cover: string | Express.Multer.File,
    file: Express.Multer.File,
  ): Promise<Music> {
    const id = uuidv4();
    let coverFileName = '';
    this.uploadFile(id, file);
    if (typeof cover === 'string') coverFileName = cover;
    else {
      this.uploadFile(id, cover);
      coverFileName = cover.originalname;
    }
    const music = await this.musicRepository.create({
      singer: musicDto.singer,
      title: musicDto.title,
      dateOut: musicDto.dateOut,
      albumId: musicDto.albumId,
      musicId: id,
      cover: coverFileName,
      file: file.originalname,
    });
    // const dto = plainToClass(MusicDto, music);
    return music;
  }

  async updateMusic(
    musicId: string,
    musicDto: MusicDto,
    cover: Express.Multer.File | null,
    file: Express.Multer.File | null,
  ): Promise<Music> {
    if (file != null) this.uploadFile(musicId, file);
    if (cover != null) this.uploadFile(musicId, cover);
    const music = await this.musicRepository.findOneAndUpdate(
      {
        musicId,
      },
      musicDto,
    );
    // const dto = plainToClass(MusicDto, music);
    return music;
  }

  async deleteMusic(musicId: string): Promise<Music> {
    const music = await this.musicRepository.findOneAndDelete({
      musicId,
    });
    fs.unlinkSync(join(pathUPLOAD, music.file));
    if (music.cover.split('.mp3')[0] == musicId)
      fs.unlinkSync(join(pathUPLOAD, music.cover));
    console.info(`Musics ${musicId} is deleted`);
    return music;
  }
  private uploadFile(id: string, file: Express.Multer.File | null): void {
    if (!file) {
      console.error('not file found');
      return;
    }
    file.originalname = `${id}.${file.mimetype.split('/')[1]}`;
    fs.writeFileSync(join(pathUPLOAD, file.originalname), file.buffer);
  }
}
