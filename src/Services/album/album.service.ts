/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { AlbumDto } from '../../DTO/album/album.dto';
import { AlbumRepository } from '../../Repositories/album.repository';
import { v4 as uuidv4 } from 'uuid';
import { plainToClass } from 'class-transformer';
import { Album } from '../../Schemas/album.schema';
import { MusicService } from '../music.service';
import * as fs from 'fs';
import path, { join } from 'path';
import { MusicDto } from 'src/DTO/music.dto';
let pathUPLOAD = 'D:\\Musica.P2\\Musica.Nest\\files\\';
if ('FILES_PVC' in process.env) pathUPLOAD = process.env['FILES_PVC'];
@Injectable()
export class AlbumService {
  constructor(
    private readonly albumRepository: AlbumRepository,
    private readonly musicService: MusicService,
  ) {}

  async createAlbum(
    createAlbumDto: AlbumDto,
    cover: Express.Multer.File,
    musics: Express.Multer.File[],
  ): Promise<Album> {
    if (createAlbumDto.musics_str) {
      const data = [];
      createAlbumDto.musics_str.forEach((value) => {
        data.push(JSON.parse(value));
      });
      createAlbumDto.musics = data;
    }
    const id = uuidv4();
    // UPLOAD FILE
    this.UploadFile(id, cover);
    // UPLOAD FILE
    const album = await this.albumRepository.create({
      albumId: id,
      titre: createAlbumDto.titre,
      dateDeSortie: createAlbumDto.dateDeSortie,
      artistes: createAlbumDto.artistes,
      description: createAlbumDto.description,
      cover: cover.originalname,
    });
    // INSERT ALBUM MUSIC IN MUSIC
    this.InsertMusicAlbum(
      album.albumId,
      cover.originalname,
      createAlbumDto.musics,
      musics,
    );
    // const dto = plainToClass(AlbumDto, album);
    // return dto;
    return album;
  }

  async updateAlbum(
    albumId: string,
    updateAlbumDto: AlbumDto,
    cover: Express.Multer.File | null,
  ): Promise<Album> {
    if (cover != null) this.UploadFile(albumId, cover);
    const album = await this.albumRepository.findOneAndUpdate(
      { albumId },
      updateAlbumDto,
    );
    return album;
  }

  async getAllAlbums(): Promise<Album[]> {
    const albums = await this.albumRepository.find({});
    // const dto = plainToClass(AlbumDto, albums);
    return albums;
  }

  async getAlbum(albumId: string): Promise<Album> {
    const album = await this.albumRepository.findOne({ albumId });
    // const dto = plainToClass(AlbumDto, album);
    return album;
  }

  async deleteAlbum(albumId: string): Promise<Album> {
    const album = await this.albumRepository.findOneAndDelete({ albumId });
    fs.unlinkSync(join(pathUPLOAD, album.cover));
    // const dto = plainToClass(AlbumDto, album);
    // DELETE ALL MUSICS OF ALBUM
    const musics = await this.musicService.getMusicsByAlbumId(albumId);
    musics.forEach((values) => {
      this.musicService.deleteMusic(values.musicId);
    });

    return album;
  }
  private UploadFile(id: string, cover: Express.Multer.File | null): void {
    if (cover) {
      // UPDATE COVER
      cover.originalname = `${id}.${cover.mimetype.split('/')[1]}`;
      fs.writeFileSync(join(pathUPLOAD, cover.originalname), cover.buffer);
    } else {
      console.error('NOT FILE FOUND HERE');
    }
  }
  private InsertMusicAlbum(
    albumId: string,
    coverFileName: string,
    musicsDto: MusicDto[],
    files: Express.Multer.File[],
  ): void {
    if (
      files.length == 0 ||
      musicsDto.length == 0 ||
      musicsDto.length != files.length
    ) {
      console.error('music and file music not found');
      return;
    }
    for (let i = 0; i < musicsDto.length; i++) {
      const music = musicsDto[i];
      music.albumId = albumId;
      const musicCreated = this.musicService.createMusic(
        music,
        coverFileName,
        files[i],
      );
      console.info(`this music is created : ${musicCreated}`);
    }
  }
}
