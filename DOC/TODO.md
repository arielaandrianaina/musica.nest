## Upload file

## Schemas

Les Schemas qui ont besoin d'etre modifier:

1. <b>music.schema.ts</b> : <br>
   Ajouter deux nouvelles attribut :

```
    file : string;
    cover : string;
```

file pour retenir le fichier MP3 <br>
cover pour retenir le fichier IMAGE

2. <b>album.schema.ts</b> : <br>
   Ajouter une nouvelle attribut :

```
    cover : string;
```

cover pour retenir le fichier IMAGE

## Controller

Les controllers qui ont besoin d'etre modifier :

1. <b>album.controller.ts</b> <br>
   On pourra uploader 1 fichier IMAGE qui est la pochette et des fichiers MP3 pour les musics <br>

```
    @Post()
    @HttpCode(201)
    @UseInterceptors(FileFieldsInterceptor([
        { name: 'cover', maxCount: 1 },
        { name: 'musics', minCount: 1 },
    ]))
    async createAlbum(
        @UploadedFiles() files: { cover?: Express.Multer.File[], musics?: Express.Multer.File[] },
        @Body() createAlbumDto: AlbumDto
        ): Promise<Album>
```
<b>cover</b> sera à la fois l'image de la couverture de l'album et à la fois l'image par défaut des musics

2. <b>music.controller.ts</b>

```
    @Post()
    @HttpCode(201)
    @UseInterceptors(FileFieldsInterceptor([
        { name: 'cover', maxCount: 1 },
        { name: 'musics', maxCount: 1 },
    ]))
    async createMusic(
        @UploadedFiles() files: { cover?: Express.Multer.File[], musics?: Express.Multer.File[] },
        @Body() body: MusicDto
        ): Promise<Music>

```
**Remarque** <br>
- Voir la documentation officielle pour modifier les services correspondant qui sont les fichiers : <b>album.service.ts</b> et <b>music.service.ts</b> <br>
Et voir la documentation aussi pour la sauvegarde des fichiers sur le serveurs et autre configuration : <a href="https://docs.nestjs.com/techniques/file-upload" >DOCUMENATION</a>
- Mettre à jour cette documentation après modification du code 
