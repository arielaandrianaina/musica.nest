/* eslint-disable prettier/prettier */
import { INestApplication } from '@nestjs/common';
import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { Connection, Model, connect } from 'mongoose';
import { Music, MusicSchema } from '../src/Schemas/music.schema';
import * as request from 'supertest';
import { MusicController } from '../src/Controller/music/music.controller';
import { MusicService } from '../src/Services/music.service';
import { MusicRepository } from '../src/Repositories/music.repository';
import { response } from 'express';

describe('Test music', () => {
  let app: INestApplication;
  let mongod: MongoMemoryServer;
  let mongoConnection: Connection;
  let musiceModel: Model<Music>;
  let body: any;

  beforeAll(async () => {
    mongod = await MongoMemoryServer.create();
    const uri = mongod.getUri();
    mongoConnection = (await connect(uri)).connection;
    musiceModel = mongoConnection.model(Music.name, MusicSchema);
    const moduleFixture: TestingModule = await Test.createTestingModule({
      controllers: [MusicController],
      providers: [
        MusicService,
        MusicRepository,
        { provide: getModelToken(Music.name), useValue: musiceModel },
      ],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  afterAll(async () => {
    await mongoConnection.dropDatabase();
    await mongoConnection.close();
    await mongod.stop();
    await app.close()
  });

  describe('Create music', () => {
    //jest.setTimeout(5000*10);
    it(`/POST music`, () => {
      // const dto =[
      //   {
      //     singer: "Tence mena",
      //     title : "400 volts",
      //     dateOut: "2009-04-25"
      //   },
      //   {
      //     singer: "Tence mainty",
      //     title : "1000 volts",
      //     dateOut: "2013-05-29"
      //   }
      // ]
      const dto = {
        singer: 'Tence mena',
        title: '400 volts',
        dateOut: '2009-04-25',
      };

      return request(app.getHttpServer())
        .post('/music')
        .send(dto)
        .expect(201)
        .then((response) => {
          body = response.body;
          console.log(response.body);
        });
    });
  });

  describe('get one', () => {
    // console.log(body)
    it(`/GET music`, () => {
      return request(app.getHttpServer())
        .get('/music/69')
        .expect(200)
        // .then((response) => {
        //   console.log(response.body);
        // });
    });
  });

  describe('get music list', () => {
    it('/GET list', () => {
      return request(app.getHttpServer())
        .get('/music')
        .expect(200)
        // .then((response) => {
        //   console.log(response.body.length);
        // });
    });
  });

  describe('update a music', () => {
    it('/PUT music', () => {
      return request(app.getHttpServer())
        .put('/music/1')
        .expect(200)
        // .then((response) => {
        //   console.log(response.body);
        // });
    });
  });
});
