/* eslint-disable prettier/prettier */
import { INestApplication } from '@nestjs/common';
import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { Connection, Model, connect } from 'mongoose';
import { AlbumController } from '../src/Controller/album/album.controller';
import { AlbumRepository } from '../src/Repositories/album.repository';
import { AlbumSchema } from '../src/Schemas/album.schema';
import { Album } from '../src/Schemas/album.schema';
import { AlbumService } from '../src/Services/album/album.service';

describe('Test album', () => {
  let app: INestApplication;
  let mongod: MongoMemoryServer;
  let mongoConnection: Connection;
  let albumModel: Model<Album>;

  beforeAll(async () => {
    mongod = await MongoMemoryServer.create();
    const uri = mongod.getUri();
    mongoConnection = (await connect(uri)).connection;
    albumModel = mongoConnection.model(Album.name, AlbumSchema);
    const moduleFixture: TestingModule = await Test.createTestingModule({
      controllers: [AlbumController],
      providers: [
        AlbumService,
        AlbumRepository,
        { provide: getModelToken(Album.name), useValue: albumModel },
      ],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  afterAll(async () => {
    await mongoConnection.dropDatabase();
    await mongoConnection.close();
    await mongod.stop();
    await app.close();
  });

  describe('Create album', () => {
    it('/POST album', () => {
      const dto = {
        titre: 'Nemany',
        dateDeSortie: '2023-01-25',
        artistes: ['Mahatoky', 'Ramora'],
        descritption: 'Te ho artiste',
      };
      return request(app.getHttpServer())
            .post('/album')
            .send(dto)
            .expect(201)
            .then((response) =>{
              console.log(response.body);
            } );       
    });
  });
  
  describe('get one album', () => {
    it('/GET album', () => {
      const response = request(app.getHttpServer)
            .get('/album/69')
            .expect(200);
        //console.log(response);
    });
  });

  describe('get list album', () => {
    it('/GET list', () => {
      const response = request(app.getHttpServer())
            .get('/album')
            .expect(200);
        // console.log(response);
    });
  });

  describe('Uptade album', () => {
    it('/PUT album',  () => {
      const response =  request(app.getHttpServer)
            .put('/album/69')
            .expect(200);
        // console.log(response);
    });
  });

  describe('Delete album', () => {
    it('/Del album',  () => {
      const response =  request(app.getHttpServer())
            .delete('/album/69')
            .expect(200);
        // console.log(response);
    });
  });
});
